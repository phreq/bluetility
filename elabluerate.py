import argparse
import csv 
from csv import DictReader
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description='PlotBlue utility')
parser.add_argument('-f', '--file', dest='filepath', type=str, help='Path of the acquisition file')
args = parser.parse_args()

serials = []

data_file = open(args.filepath, "r")
csv_dict_reader = DictReader(data_file)
for row in csv_dict_reader:
    if not row['serial'] in serials:
        serials.append(str(row['serial']))

points = {}
for serial in serials:
    points[serial] = []
    i = 0
    data_file.seek(0)
    for row in csv_dict_reader:
        if serial == row['serial']:
            points[serial].append(int(row['rssi']))
            i+=1

############### DEBUG ###############
mean = np.mean(list(points.values()))
variance = np.var(list(points.values()))
std_dev = np.std(list(points.values()))

print(points)
print("\n\n")
print("Considered serials in file: ", end=" ")
print(*serials, sep=", ")
print("Mean: " + str(mean))
print("Variance: " + str(variance))
print("Standard deviation: " + str(std_dev))

for serial in serials:
    data_file.seek(0)
