import argparse
import csv 
from csv import DictReader
import matplotlib.pyplot as plt 
import pandas as pd
import numpy as np
from matplotlib.pyplot import cm

parser = argparse.ArgumentParser(description='PlotBlue utility')
parser.add_argument('-f', '--file', dest='filepath', type=str, help='Path of the acquisition file')
args = parser.parse_args()

serials = []

data_file = open(args.filepath, "r")
csv_dict_reader = DictReader(data_file)
for row in csv_dict_reader:
    if not row['serial'] in serials:
        serials.append(str(row['serial']))
print(serials)

color_list = iter(cm.rainbow(np.linspace(0, 1, len(serials))))

points = {}
for serial in serials:
    points[serial] = []
    i = 0
    data_file.seek(0)
    for row in csv_dict_reader:
        if serial == row['serial']:
            points[serial].append(int(row['rssi']))
            i+=1

############### DEBUG
print(points)
print("\n\n")

means = {}
for serial in serials:
    means[serial] = [np.mean(points[serial][:-1])]

print(means)

for serial in serials:
    color = next(color_list)
    data_file.seek(0)
    plt.plot (
        points[serial],
        ".-",
        c=color,
        label=serial
    )
    plt.axhline(y=means[serial], linestyle='-', c=color)

plt.ylabel('rssi')
plt.xlabel('timestamp')
plt.legend()
plt.show()
data_file.close()
