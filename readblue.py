import os
import requests
import argparse
import json
import csv 
import time
from datetime import datetime
import configparser

#---------------------------------------------------------------
parser = argparse.ArgumentParser(description='ReadBlue utility')
parser.add_argument('-a', '--address', dest='address', type=str, help='Address of the BlueUp Gateway')
parser.add_argument('-d', '--dir', dest='save_dir', type=str, help='Path of the dir for saving data')
parser.add_argument('-r', '--readcount', dest='read_count', type=int, help='How many times the script has to get the values from the API (defaults to 40 times if not set)')
parser.add_argument('-t', '--sleep', dest='sleep_time', type=int, help="Sleep time between requests (defaults to 2 seconds if not set)")
parser.add_argument('-c', '--config', dest='config_path', type=str, help="Path of the configuration file")
args = parser.parse_args()

#---------------------------------------------------------------
if args.config_path:
    config = configparser.ConfigParser()
    config.read(args.config_path)
    args.read_count = int(config['Parameters']['ReadCount'])
    args.sleep_time = int(config['Parameters']['SleepTime'])
    args.address = config['Parameters']['Address']
    args.save_dir = config['Parameters']['PathToSave']
    if args.save_dir:
        if not os.path.isdir(args.save_dir):
            os.makedirs(args.save_dir)
        else:
            print("Not creating folder " + args.save_dir + " because it already exists")
else:
    if not args.save_dir:
        args.save_dir = "./"

    if not args.read_count:
        args.read_count = 60

    if not args.sleep_time:
        args.sleep_time = 3

#---------------------------------------------------------------
today_date = datetime.now()
today_date = today_date.strftime("%d_%m_%Y_at_%H_%M")

output_file = open(args.save_dir + today_date + ".csv", 'w')
writer = csv.writer(output_file)

#---------------------------------------------------------------

print("ETA: " + str(args.sleep_time*args.read_count) + " seconds")

output_file.write('timestamp,serial,rssi,s_rssi\n')
for i in range(args.read_count):
    request = requests.get('http://' + args.address + '/api/beacons/blueup')
    request_json = request.json()

    for beacon in request_json['beacons']:
        print("Time stamp " + str(beacon['timestamp']))
        print("Serial " + str(beacon['serial']))
        print("RSSI " + str(beacon['rssi']) + " dBm")
        print("Safety RSSI " + str(beacon['safety']['rssi']) + " dBm")

        writer.writerow (
            [
                beacon['timestamp'],
                beacon['serial'],
                beacon['rssi'],
                beacon['safety']['rssi']
            ]
        )
    
    time.sleep(args.sleep_time)

output_file.close()
